// Lắng nghe sự kiện push từ server
self.addEventListener('push', function (event) {
  if (!(self.Notification && self.Notification.permission === 'granted')) {
    return
  }
  if (event.data) {
    var msg = event.data.json()
    console.log('Push Received: ', msg)
    // Gửi data đến client
    sendMessageToClient(msg)
    // Hiển thị thông báo
    event.waitUntil(
      self.registration.showNotification(msg.title, {
        body: msg.body,
        icon: msg.icon,
        image: msg.icon,
        actions: [
          {
            title: 'say hi'
          }
        ]
      })
    )
  }
})

self.addEventListener('activate', async function (event) {
  event.waitUntil(self.clients.claim())
  const clients = await self.clients.matchAll()
  console.log('clients', clients)
})

async function sendMessageToClient(msg) {
  // get all open clients of this service worker
  const clients = await self.clients.matchAll()
  // send a message to each client
  clients.forEach((client) => {
    console.log('client', client)
    client.postMessage(msg)
  })
}
