import { axios } from '../index.config'

export const notificationApi = {
  unsubscribe: (endpoint: string) => axios.post('/noti/unsubscribe', { endpoint }),
  subscribe: (data: any) => axios.post('/noti/subscribe', data)
}
